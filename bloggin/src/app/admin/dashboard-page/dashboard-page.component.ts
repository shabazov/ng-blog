import { AlertService } from './../shared/services/alert.service';
import { Post } from './../shared/interfaces';
import { PostsService } from './../shared/services/posts.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  posts: Post[] = [];
  postSubscription: Subscription;
  deleteSubscription: Subscription;
  searchString: string = '';

  constructor(private postsService: PostsService, private alertService: AlertService) { }

  ngOnInit(): void {
    this.postSubscription = this.postsService.getAll().subscribe(posts => {
      this.posts = posts
    })
  }
  remove(id: string) {
    this.deleteSubscription =  this.postsService.remove(id).subscribe(() => {
      this.posts = this.posts.filter(post => post.id !== id);
      this.alertService.danger('Post deleted.');
    })
  }

  ngOnDestroy(): void {
    if(this.postSubscription) {
      this.postSubscription.unsubscribe()
    }
    if(this.deleteSubscription) {
      this.deleteSubscription.unsubscribe()
    }
  }


}
