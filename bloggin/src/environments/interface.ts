
export interface Environment {
  apiKey: string;
  production: boolean;
  fireBlink: string;
}