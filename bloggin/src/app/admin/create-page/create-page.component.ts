import { AlertService } from './../shared/services/alert.service';
import { PostsService } from './../shared/services/posts.service';
import { Post } from './../shared/interfaces';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fB: FormBuilder,
    private postService: PostsService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.form = this.fB.group({
      title: ['', Validators.required],
      text: ['', Validators.required],
      author: ['', Validators.required]
    })
  }

  submit() {
    if(this.form.invalid) {
      return
    }

    const post: Post = {
      title: this.form.value.title,
      text: this.form.value.text,
      author: this.form.value.author,
      date: new Date()
    }

    this.postService.create(post).subscribe(() => {
      this.form.reset();
      this.alertService.success('Post was created successfully!')
    })
        
  }

}
