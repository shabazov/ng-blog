import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { QuillModule } from 'ngx-quill';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    QuillModule.forRoot()
  ],
  exports: [
    HttpClientModule,
    QuillModule
  ],
  declarations: []
})

export class SharedModule {}